from rest_framework import generics
from rest_framework import viewsets 
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Producto, Categoria, SubCategoria
from .serializers import ProductoSerializer, CategoriaSerializer, SubCategoriaSerializer

from django.contrib.auth.models import User
from .serializers import UserSerializer

from django.contrib.auth import authenticate


 # ============= GENERICS

class ProductoList(generics.ListCreateAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
 
class ProductoDetalle(generics.RetrieveDestroyAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer

class CategoriaList(generics.ListCreateAPIView):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer
 
class CategoriaDetalle(generics.RetrieveDestroyAPIView):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer

class SubCategoriaList(generics.ListCreateAPIView):
    queryset = SubCategoria.objects.all()
    serializer_class = SubCategoriaSerializer
 
class SubCategoriaDetalle(generics.RetrieveDestroyAPIView):
    queryset = SubCategoria.objects.all()
    serializer_class = SubCategoriaSerializer

class CategoriaSave(generics.CreateAPIView):
    serializer_class = CategoriaSerializer
 
class SubCategoriaSave(generics.CreateAPIView):
        serializer_class = SubCategoriaSerializer

class ProductoSave(generics.CreateAPIView):
        serializer_class = ProductoSerializer


# ============= ViewSets
class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer

class CategoriaViewSet(viewsets.ModelViewSet):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer

class SubCategoriaViewSet(viewsets.ModelViewSet):
    queryset = SubCategoria.objects.all()
    serializer_class = SubCategoriaSerializer

# ============= Usuarios
class UserCreate(generics.CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerializer

class LoginView(APIView):
    permission_classes = ()
 
    def post(self, request,):
        username = request.data.get("username")
        password = request.data.get("password")
        user = authenticate(username=username, password=password)
        if user:
            return Response({"token": user.auth_token.key})
        else:
            return Response({"error": "Wrong Credentials"}, status=status.HTTP_400_BAD_REQUEST)