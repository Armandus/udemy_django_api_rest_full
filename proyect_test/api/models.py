﻿from django.db import models

# Create your models here.
class Categoria(models.Model):
	descripcion = models.CharField(
		max_length=100,
		help_text='Descripción de la categoria',
		unique = True
	)
	
	def __str__(self):
		return "{}".format(self.descripcion)
		
	class Meta:
		verbose_name_plural="Cateogias"
		

class SubCategoria(models.Model):
	categoria = models.ForeignKey(Categoria, on_delete = models.CASCADE)
	descripcion = models.CharField(
		max_length=100,
		help_text='Descripción de la SubCategoria'
	)
	
	def __str__(self):
		return("{}.{}".format(self.categoria.descripcion, self.descripcion))
		
	class Meta:
		verbose_name_plural = "SubCategorias"
		unique_together = ('categoria', 'descripcion')
		
		
class Producto(models.Model):
	subcategoria = models.ForeignKey(SubCategoria, on_delete = models.CASCADE)
	descripcion = models.CharField(
		max_length = 100,
		help_text = 'Descripción del Producto',
		unique = True
	)
	
	def __str__(self):
		return("{}".format(self.descripcion))
		
	class Meta:
		verbose_name_plural = "Productos"