from django.urls import path
from api.apiviews import ProductoList, ProductoDetalle, ProductoSave
from api.apiviews import CategoriaList, CategoriaDetalle, CategoriaSave
from api.apiviews import SubCategoriaList, SubCategoriaDetalle, SubCategoriaSave

from api.apiviews import ProductoViewSet, CategoriaViewSet, SubCategoriaViewSet
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views

from api.apiviews import UserCreate
from api.apiviews import LoginView

router = DefaultRouter()

urlpatterns = [
    path('v1/productos/', ProductoList.as_view(),name='producto_list' ),
    path('v1/productos/<int:pk>', ProductoDetalle.as_view(),name='producto_detalle' ),
	path('v1/productos/save/', ProductoSave.as_view(),name='producto_save' ),
	path('v1/categorias/', CategoriaList.as_view(),name='categoria_list' ),
	path('v1/categorias/<int:pk>', CategoriaDetalle.as_view(),name='categoria_detalle'),
	path('v1/categorias/save/', CategoriaSave.as_view(),name='categoria_save' ),
	path('v1/subcategorias/', SubCategoriaList.as_view(),name='subcategoria_list' ),
	path('v1/subcategorias/<int:pk>', SubCategoriaDetalle.as_view(),name='subcategoria_detalle'),
	path('v1/subcategorias/save/', SubCategoriaSave.as_view(),name='subcategoria_save' ),
	path('v3/usuarios/', UserCreate.as_view(), name='usuario_crear'),
	path("v4/login/", LoginView.as_view(), name="login"),
	path("v4/login-drf/", views.obtain_auth_token , name="login_drf"),
]

urlpatterns += router.urls
router.register('v2/productos', ProductoViewSet, basename='productos')
urlpatterns += router.urls
router.register('v2/categorias', CategoriaViewSet, basename='categorias')
urlpatterns += router.urls
router.register('v2/subcategorias', SubCategoriaViewSet, basename='subcategorias')
urlpatterns += router.urls



