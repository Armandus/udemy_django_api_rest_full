from django.urls import path
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse

def ejemplo(request):
	data = {'miResultado':'si dentro'}
	return JsonResponse(data)

urlpatterns = [
	path('resultado/', ejemplo, name = 'ejemplo')	
]